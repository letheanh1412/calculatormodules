package calculatorTest;

import calculator.inFixtoPostFix;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class inFixtoPostFixTest {
    @Test

    //Test toán tử
    public void isOperatorTest(){
        inFixtoPostFix S= new inFixtoPostFix();
        boolean result;
        result= S.isOperator('+');
        assertEquals(true, result);
        result= S.isOperator('-');
        assertEquals(true, result);
        result= S.isOperator('*');
        assertEquals(true, result);
        result= S.isOperator('/');
        assertEquals(true, result);
        result= S.isOperator('$');
        assertEquals(false, result);
        result= S.isOperator('!');
        assertEquals(false, result);
        result= S.isOperator('@');
        assertEquals(false, result);
        result= S.isOperator('#');
        assertEquals(false, result);
        result= S.isOperator('%');
        assertEquals(false, result);
        result= S.isOperator('&');
        assertEquals(false, result);
        result= S.isOperator('a');
        assertEquals(false, result);
    }

    //Test độ ưu tiên của toán tử
    @Test
    public void priorityTest() {
        inFixtoPostFix S= new inFixtoPostFix();
        int result;
        result =S.priority('+');
        assertEquals(1, result);
        result = S.priority('-');
        assertEquals(1, result);
        result = S.priority('/');
        assertEquals(2, result);
        result = S.priority('*');
        assertEquals(2, result);
        result = S.priority('x');
        assertEquals(0, result);
        result = S.priority('!');
        assertEquals(0, result);
        result = S.priority('@');
        assertEquals(0, result);
        result = S.priority('&');
        assertEquals(0, result);
        result = S.priority('%');
        assertEquals(0, result);
    }

    //Test chuỗi nhập vào
    @Test
    public void checkCharacterTest() {
        inFixtoPostFix S = new inFixtoPostFix();
        boolean result;
        result  = S.checkCharacter("5-1+4*4/2");
        assertEquals(true, result);
        result = S.checkCharacter("1+2(3+4)*5/6");
        assertEquals(true, result);
        result = S.checkCharacter("1111111+42342343*(223+434)-432423/4324");
        assertEquals(true, result);
        result = S.checkCharacter("abcxyz");
        assertEquals(false, result);
        result = S.checkCharacter("11=*45-34%9");
        assertEquals(false,result);
        result = S.checkCharacter("11a+434-343$-2222");
        assertEquals(false,result);
        result = S.checkCharacter("4432432-4343+4545*434c");
        assertEquals(false,result);
    }

    //Test toán tử nhập vào
    @Test
    public void checkOperatorTest() {
        inFixtoPostFix S = new inFixtoPostFix();
        boolean result;
        result = S.checkOperator("1+2*3/4");
        assertEquals(true, result);
        result = S.checkOperator("1+(4-2)*5+4/2");
        assertEquals(true, result);
        result = S.checkOperator("1++2");
        assertEquals(false, result);
        result = S.checkOperator("13--1");
        assertEquals(false, result);
        result = S.checkOperator("12**2");
        assertEquals(false, result);
        result = S.checkOperator("123213//332");
        assertEquals(false, result);
        result = S.checkOperator("1232-+222");
        assertEquals(false, result);
        result = S.checkOperator("8989878*/232");
        assertEquals(false, result);
        result = S.checkOperator("4742347834783497847-*9483294");
        assertEquals(false, result);
    }

    //Test dấu ngoặc
    @Test
    public void checkParenthesesTest() {
        inFixtoPostFix S = new inFixtoPostFix();
        boolean result;
        result = S.checkParentheses("(2)+(1)");
        assertEquals(true, result);
        result = S.checkParentheses("(9)");
        assertEquals(true, result);
        result = S.checkParentheses("((9)");
        assertEquals(false,result);
        result=S.checkParentheses("((((((69))))))");
        assertEquals(true, result);
        result=S.checkParentheses("(+69)");
        assertEquals(false, result);
        result=S.checkParentheses("(69-)");
        assertEquals(false, result);
        result=S.checkParentheses("((2+456)-(4467))");
        assertEquals(true, result);
        result=S.checkParentheses("(2)+(7");
        assertEquals(false, result);
        result=S.checkParentheses("(256+5)*4");
        assertEquals(true, result);
        result=S.checkParentheses("((2)+(5))+97+((8)-6)");
        assertEquals(true, result);
        result=S.checkParentheses("((69)+)");
        assertEquals(false, result);
        result=S.checkParentheses("((*69))");
        assertEquals(false, result);
        result=S.checkParentheses("((28+)+)");
        assertEquals(false, result);
        result=S.checkParentheses(")432423+4348(");
        assertEquals(false, result);
        result=S.checkParentheses("4324-432432)");
        assertEquals(false, result);
    }

    //Test chuẩn hóa biểu thức nhập vào
    @Test
    public void processStringTest() {
        inFixtoPostFix S = new inFixtoPostFix();
        String[] proFix = S.processString("1+2*3-4");
        String[] expression1={"1","+","2","*","3","-","4"};
        assertArrayEquals(expression1, proFix);
        proFix = S.processString("(1)*4+5");
        String[] expression2={"(","1",")","*","4","+","5"};
        assertArrayEquals(expression2, proFix);
        proFix = S.processString("(321312)*4324234+56464");
        String[] expression3={"(","321312",")","*","4324234","+","56464"};
        assertArrayEquals(expression3, proFix);
    }

    //Test chuẩn hóa biểu thức trung tố sang hậu tố
    @Test
    public void postFix() {
        inFixtoPostFix S = new inFixtoPostFix();
        String[] expression1 = {"1", "+", "2", "*", "3", "-", "5"};
        String[] proFix = S.postFix(expression1);
        List<String> list = new LinkedList<String>(Arrays.asList(proFix));
        list.remove(0);
        String[] expression2 = {"1", "2", "3", "*", "+", "5", "-"};
        assertArrayEquals((expression2), list.toArray());
    }

    //Test kết quả chuỗi hậu tố
    @Test
    public void valueMathTest() {
        inFixtoPostFix S = new inFixtoPostFix();
            String[] string = {" ", "2423434", "2232323", "786", "*", "+", "577777", "-"};
            String result = S.valueMath(string);
            assertEquals("1.756451471E9", result);
    }

}
